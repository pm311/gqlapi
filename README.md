# gqlapi 
### features
##### Create a new quiz. Teachers should be able to create a new quiz by providing thequiz name along with a list of questions and their corresponding answers.
```javascript
mutation { createQuiz(input:{ name: "name of quiz"  }) {id}}
mutation { createQuestion(input: { content: "content of question", quizId: 1 }) {id}}
mutation { createAnswer(input: { content: "content of answer", correct: true, questionId: 17 }) {id}}
```
each of above mutations returns id of generated entity assigned by database
##### Check answers. Students should be able to submit their answers for a quiz and receive the results.
questions may have single correct answer or multiple correct answers

quizId - id of currently solved quiz
answersIds - IDs marked by student


```javascript
query { getQuizResult(input:{quizId:2,answersIds:[1,2,5,7,12,14,23]})}
```
```javascript
{  "data": {    "getQuizResult": 3  }}
```
##### Get questions for a quiz. Students should be able to fetch the questions for a specific quiz.

```javascript
query { getWholeQuiz(input:{id:1})}
```
returns csv style string of all questions for given quiz Id and every answer assigned to those questions


### install 
on debian 12

##### execute this using root account

```sh
apt update
apt install docker.io npm git
docker pull mariadb:10.11.4
docker run --name mar -e MYSQL_ROOT_PASSWORD=pass -p 3306:3306 -d docker.io/library/mariadb:10.11.4
docker exec -it mar bash
mariadb -p
```
enter 'pass'
```sql
create database test1;
create user 'uzytkownik'@172.17.0.1 IDENTIFIED by 'pass';
grant all privileges on test1.* to 'uzytkownik'@172.17.0.1;
flush privileges;
```

##### then execute this using nonroot account

```sh
git clone https://gitlab.com/pm311/gqlapi
cd gqlapi
npm install @nestjs/apollo @nestjs/common @nestjs/graphql @nestjs/testing @nestjs/typeorm mysql typeorm
npm start
```
