import { ObjectType, Field, ID, InputType } from '@nestjs/graphql';
import { QuestionDTO } from '../question/question.dto';

@ObjectType()
export class AnswerDTO {
  @Field(() => ID)
  id: number;

  @Field()
  content: string;
  
  @Field()
  correct: boolean;

  @Field(() => QuestionDTO)
  question: QuestionDTO;
}

@InputType()
export class CreateAnswerInput {
  @Field()
  content: string;
  
  @Field()
  correct: boolean;

  @Field()
  questionId: number;
}
