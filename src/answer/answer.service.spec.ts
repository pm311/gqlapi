import { Test, TestingModule } from '@nestjs/testing';
import { AnswerService } from './answer.service';
import { Answer } from './answer.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { QuestionService } from '../question/question.service';
import { Question } from '../question/question.entity';
import { QuizService } from '../quiz/quiz.service';
import { Quiz } from '../quiz/quiz.entity';
	
const question1 = {
	content: 'this is a unit test',
	quizId: 111};
const answer1 = {	
	content: 'this is a unit test',
	correct: true,
	questionId: 111,	};
const quiz1 = {
	id: 111,
	name: 'this is a unit test'	};

describe('AnswerService', () => {
  let service: AnswerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuestionService,AnswerService,QuizService,
			{
			provide: getRepositoryToken(Answer),
			useValue: {
				save: jest.fn().mockResolvedValue(answer1)
				},
			},
			{
			provide: getRepositoryToken(Question),
			useValue: {
				findById: jest.fn().mockResolvedValue(question1),				
				findOneBy: jest.fn().mockResolvedValue(question1)
				},
				},
			{
			provide: getRepositoryToken(Quiz),
			useValue: {
				findById: jest.fn().mockResolvedValue(quiz1),				
				findOneBy: jest.fn().mockResolvedValue(quiz1)
				},
				}
			],
	}).compile();

    service = module.get<AnswerService>(AnswerService);
  });

	describe('createAnswer()', () => {
		it('should create an answer', () => {
			const answer1 = {	
				content: 'this is a unit test',
				correct: true,
				questionId: 111,	};
		expect(
			service.createAnswer(answer1),)
				.resolves.toEqual(answer1);		
		});		});		});
