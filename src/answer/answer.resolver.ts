import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { AnswerService } from './answer.service';
import { AnswerDTO, CreateAnswerInput } from './answer.dto';

@Resolver(() => AnswerDTO)
export class AnswerResolver {
  constructor(private readonly answerService: AnswerService) {}

  @Query(() => AnswerDTO)
  async getAnswer(@Args('id') id: number): Promise<AnswerDTO> {
    return this.answerService.findById(id);
  }
  
  @Mutation(() => AnswerDTO)
  async createAnswer(@Args('input') input: CreateAnswerInput): Promise<AnswerDTO> {
    return this.answerService.createAnswer(input);
  }
}
