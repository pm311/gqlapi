import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Answer } from './answer.entity';
import { CreateAnswerInput } from './answer.dto';
import { QuestionService } from '../question/question.service';
import { Question } from '../question/question.entity';

@Injectable()
export class AnswerService {
	constructor(
	@InjectRepository(Answer)
	private readonly answerRepository: Repository<Answer>,
	private readonly questionService: QuestionService, 	
  ) {}	
	
  async findById(id: number): Promise<Answer> {
    return this.answerRepository
		.findOneBy({id:id});
  }

  async createAnswer(input: CreateAnswerInput): Promise<Answer> {
    const { content, correct, questionId } = input;
    const answer = new Answer();
	answer.content = content;	
	const question = await this.questionService.findById(questionId);
	if (!question) {
      throw new Error(`Question with ID ${questionId} not found.`);
    }
	answer.question = question;	
	answer.correct = correct;
    return this.answerRepository.save(answer);
  }
	
}
