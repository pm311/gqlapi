import { Module } from '@nestjs/common';
import { AnswerService } from './answer.service';
import { AnswerResolver } from './answer.resolver';
import { QuestionService } from '../question/question.service';
import { Question } from '../question/question.entity';
import { Quiz } from '../quiz/quiz.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Answer } from './answer.entity';
import { QuizModule } from '../quiz/quiz.module';
import { QuizService } from '../quiz/quiz.service';
import { QuestionModule } from '../question/question.module';

@Module({
  providers: [AnswerService, AnswerResolver, QuestionService, QuizService],
  imports: [
	TypeOrmModule.forFeature([Question, Answer, Quiz]),
	QuizModule, QuestionModule, Question, Quiz
  ]
})
export class AnswerModule {}
