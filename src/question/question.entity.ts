import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Quiz } from '../quiz/quiz.entity';
import { Answer } from '../answer/answer.entity';

@Entity()
export class Question {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  content: string;

  @ManyToOne(() => Quiz, quiz => quiz.questions)
  @JoinColumn({ name: 'quizId' })
  quiz: Quiz;
  
  @OneToMany(() => Answer, answer => answer.id, { nullable: true })
  answers: Answer[];
}
