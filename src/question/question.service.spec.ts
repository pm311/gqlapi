import { Test, TestingModule } from '@nestjs/testing';
import { QuestionService } from './question.service';
import { Question } from './question.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { QuizService } from '../quiz/quiz.service';
import { Quiz } from '../quiz/quiz.entity';
	
const question1 = {
	content: 'this is a unit test',
	quizId: 111};
const quiz1 = {
	id: 111,
	name: 'this is a unit test'	};

describe('QuestionService', () => {
  let service: QuestionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuestionService,QuizService,
			{
			provide: getRepositoryToken(Question),
			useValue: {
				save: jest.fn().mockResolvedValue(question1)
				},
			},
			{
			provide: getRepositoryToken(Quiz),
			useValue: {
				findById: jest.fn().mockResolvedValue(quiz1),				
				findOneBy: jest.fn().mockResolvedValue(quiz1)
				},
				}			
			],
	}).compile();

    service = module.get<QuestionService>(QuestionService);
  });

	describe('createQuestion()', () => {
		it('should create a question', () => {
			const question1 = {
				content: 'this is a unit test',
				quizId: 111};
		expect(
			service.createQuestion(question1),)
				.resolves.toEqual(question1);		
		});		});		});
