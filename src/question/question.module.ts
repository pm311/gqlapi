import { Module } from '@nestjs/common';
import { QuestionService } from './question.service';
import { QuestionResolver } from './question.resolver';
import { Question } from './question.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuizModule } from '../quiz/quiz.module';
import { QuizService } from '../quiz/quiz.service';
import { Quiz } from '../quiz/quiz.entity';

@Module({
  providers: [QuestionService, QuestionResolver,QuizService],
  imports: [
	  TypeOrmModule.forFeature([Question, Quiz]),
	  QuizModule,
	  Quiz
  ]
})
export class QuestionModule {}
