import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { QuestionService } from './question.service';
import { QuestionDTO, CreateQuestionInput } from './question.dto';

@Resolver(() => QuestionDTO)
export class QuestionResolver {
  constructor(private readonly questionService: QuestionService) {}

  @Query(() => QuestionDTO)
  async getQuestion(@Args('id') id: number): Promise<QuestionDTO> {
    return this.questionService.findById(id);
  }
  
  @Mutation(() => QuestionDTO)
  async createQuestion(@Args('input') input: CreateQuestionInput): Promise<QuestionDTO> {
    return this.questionService.createQuestion(input);
  }
}
