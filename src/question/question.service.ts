import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Question } from './question.entity';
import { CreateQuestionInput } from './question.dto';
import { QuizService } from '../quiz/quiz.service';
import { Quiz } from '../quiz/quiz.entity';

@Injectable()
export class QuestionService {
  constructor(
	private readonly quizService: QuizService, 
    @InjectRepository(Question)
    private readonly questionRepository: Repository<Question>,
  ) {}	
	
  async findById(id: number): Promise<Question> {
    return this.questionRepository
		.findOneBy({id:id});
	}

  async createQuestion(input: CreateQuestionInput): Promise<Question> {
    const { content, quizId } = input;
    const question = new Question();
	question.content = content;	
	const quiz = await this.quizService.findById(quizId);
	if (!quiz) {
      throw new Error(`Quiz with ID ${quizId} not found.`);
    }
	question.quiz = quiz;	
    return this.questionRepository.save(question);
  }
}
