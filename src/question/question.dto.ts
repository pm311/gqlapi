import { ObjectType, Field, ID, InputType } from '@nestjs/graphql';
import { QuizDTO } from '../quiz/quiz.dto';
import { AnswerDTO } from '../answer/answer.dto';

@ObjectType()
export class QuestionDTO {
  @Field(() => ID)
  id: number;

  @Field()
  content: string;

  @Field(() => QuizDTO)
  quiz: QuizDTO;
  
  @Field(() => [AnswerDTO], { nullable: true })
  answers?: AnswerDTO[];
}

@InputType()
export class CreateQuestionInput {
  @Field()
  content: string;

  @Field()
  quizId: number;
}
