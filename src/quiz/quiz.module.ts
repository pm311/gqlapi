import { Module } from '@nestjs/common';
import { QuizService } from './quiz.service';
import { QuizResolver } from './quiz.resolver';
import { Quiz } from './quiz.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  providers: [QuizService, QuizResolver],
  imports: [TypeOrmModule.forFeature([Quiz])]
})
export class QuizModule {}
