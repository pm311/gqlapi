import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Quiz } from './quiz.entity';
import { CreateQuizInput } from './quiz.dto';

@Injectable()
export class QuizService {
  constructor(
    @InjectRepository(Quiz)
    private readonly quizRepository: Repository<Quiz>,
  ) {}

  async findById(id: number): Promise<Quiz> {
    return this.quizRepository
		.findOneBy({id:id});
  }

  async createQuiz(name: string): Promise<Quiz> {
    const quiz = new Quiz();
    quiz.name = name;
    return this.quizRepository.save(quiz);
  }
}
