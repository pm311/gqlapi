import { ObjectType, Field, ID, InputType } from '@nestjs/graphql';
import { QuestionDTO } from '../question/question.dto';

@ObjectType()
export class QuizDTO {
  @Field(() => ID)
  id: number;

  @Field()
  name: string;

  @Field(() => [QuestionDTO], { nullable: true })
  questions?: QuestionDTO[];
}

@InputType()
export class CreateQuizInput {
  @Field()
  name: string;
}
