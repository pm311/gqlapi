import { Test, TestingModule } from '@nestjs/testing';
import { QuizService } from './quiz.service';
import { Quiz } from './quiz.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';

	
const quizName = 'this is unit test';

describe('QuizService', () => {
  let service: QuizService;
  let quizRepository: Repository<Quiz>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuizService,
			{
			provide: getRepositoryToken(Quiz),
			useValue: {
				save: jest.fn().mockResolvedValue(quizName)
				},
			}],
	}).compile();

    service = module.get<QuizService>(QuizService);
	quizRepository = module.get<Repository<Quiz>>(getRepositoryToken(Quiz));
  });

	describe('createQuiz()', () => {
		it('should create a quiz', () => {
			const quizName = 'this is unit test';

		expect(
			service.createQuiz(quizName),)
				.resolves.toEqual(quizName);		
		});
	});
});
