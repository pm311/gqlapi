import { Resolver, Query, Args, Mutation  } from '@nestjs/graphql';
import { QuizService } from './quiz.service';
import { QuizDTO, CreateQuizInput } from './quiz.dto';

@Resolver(() => QuizDTO)
export class QuizResolver {
  constructor(private readonly quizService: QuizService) {}

  @Query(() => QuizDTO)
  async getQuiz(@Args('id') id: number): Promise<QuizDTO> {
    return this.quizService.findById(id);
  }

  @Mutation(() => QuizDTO)
  async createQuiz(@Args('input') input: CreateQuizInput): Promise<QuizDTO> {
    return this.quizService.createQuiz(input.name);
  }
}
