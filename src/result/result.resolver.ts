import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { ResultService } from './result.service';
import { CreateResultInput, CreateWholeQuizInput } from './result.dto';
import { ResultOutputDTO } from './result.dto';

@Resolver()
export class ResultResolver {
	constructor(private readonly resultService: ResultService) {}

	
	@Query(() => Number)
	async getQuizResult(@Args('input') input: CreateResultInput):
		Promise<number> {
			const {quizId, answersIds} = input;
			return await this.resultService.giveResult(quizId,answersIds);			
	}
	
	@Query(() => String)
	async getWholeQuiz(@Args('input') input: CreateWholeQuizInput):Promise<String>{
		return await this.resultService.giveQuiz(input.id);
	}
}
