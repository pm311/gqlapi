import { Injectable } from '@nestjs/common';
import { QuizService } from '../quiz/quiz.service';
import { Quiz } from '../quiz/quiz.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Answer } from '../answer/answer.entity';
import { Question } from '../question/question.entity';
import { ResultOutputDTO } from './result.dto';

@Injectable()
export class ResultService {
	
	constructor(
    @InjectRepository(Quiz)
    private readonly quizRepository: Repository<Quiz>,
    @InjectRepository(Answer)
    private readonly answerRepository: Repository<Answer>,
	@InjectRepository(Question)
    private readonly questionRepository: Repository<Question>,
  ) {} 
  
	async giveResult (quizId: number, givenAnswers: any[]): Promise<number> {		
		const query = await this.questionRepository
			.createQueryBuilder('question')
			.where('question.quizId = :quizId', { quizId })
			.getMany();
		var result:number =0;
		const questionIds = query.map(question => question.id);			
		var array1:number[]=[];
		for (let i=0; i<givenAnswers.length;i++){
			array1[i]= parseInt(givenAnswers[i],10);
		}		
		const sortedGivenAnswers = array1.sort(function(a,b){return a-b;});	
		let allAnswerIdForQuiz: number[]=[]; 
		for (let i=0; i<questionIds.length;i++){
			var qId = questionIds[i];
			const query2 = await this.answerRepository
			.createQueryBuilder('answer')
			.where('answer.questionId = :qId', { qId })
			.getMany();
			const check = query2.map(answer => answer.id);
			allAnswerIdForQuiz.push.apply(allAnswerIdForQuiz,check);
		}		
		for (let i=0; i<sortedGivenAnswers.length;i++){			
			if (allAnswerIdForQuiz.includes(sortedGivenAnswers[i])==false){
				throw new Error(`answer with given id is not related to quiz with given id`);
				return -1;
			}
		}			
		for (let i=0; i<questionIds.length;i++){
			var currentQuestion = questionIds[i];						
			const queryBuilder = await this.answerRepository.createQueryBuilder('answer')
				.select(['answer.id']) 
				.where('answer.questionId = :currentQuestion', { currentQuestion })
				.getMany();
			//list of ids of answers for given question
			const afgq = await queryBuilder.map(answer => answer.id);		
			const query3 = await this.answerRepository
					.createQueryBuilder('answer')
					.select(['answer.correct']).getMany();
			const correctAnswers = await query3.map(answer=>answer.correct);
			for (let j=0; j<afgq.length;j++){
				var answerIsSelected:boolean=sortedGivenAnswers.includes(afgq[j]);
				var lastIteration:boolean=j===afgq.length-1;		
				if (correctAnswers[afgq[j]-1]){
					if (answerIsSelected){
						if (lastIteration){
							result++;
							break;
						}
					}
					else{
							break;
					}
				}
				else{
					if (!answerIsSelected){
						if (lastIteration){
							result++;
							break;
						}
					}
					else{
							break;
					}
				}
			}			
		}
		return result;
	}
	
	async giveQuiz(quizId:number): Promise<String>{		
		const results = await this.questionRepository
			.query('select question.id as qi , question.content as qc, answer.id as ai, answer.content as ac from'+
			' question join answer on question.id = answer.questionId where question.quizId = ?',
			[quizId]);		
		const columnNames = Object.keys(results[0] || {}).join(',');		
		const rows = results.map((row) => Object.values(row).join(','));
		const csvString = `${columnNames}\n${rows.join('\n')}`;
		return csvString;
	}	
}
