import { Module } from '@nestjs/common';
import { ResultService } from './result.service';
import { ResultResolver } from './result.resolver';
import { AnswerService } from '../answer/answer.service';
import { QuestionService } from '../question/question.service';
import { Question } from '../question/question.entity';
import { Quiz } from '../quiz/quiz.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Answer } from '../answer/answer.entity';
import { QuizModule } from '../quiz/quiz.module';
import { QuizService } from '../quiz/quiz.service';
import { QuestionModule } from '../question/question.module';
import { AnswerModule } from '../answer/answer.module';

@Module({
  providers: [ResultService, ResultResolver, QuestionService, QuizService],
  imports: [
	TypeOrmModule.forFeature([Question, Answer, Quiz]),
	QuizModule, QuestionModule, Question, Quiz,Answer,AnswerModule
  ]
})
export class ResultModule {}
