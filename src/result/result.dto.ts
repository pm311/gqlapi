import { ObjectType, Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateResultInput {
	@Field(()=>ID)
	quizId: number;
	
	@Field(()=>[ID])
	answersIds: number[];
}

@InputType()
export class CreateWholeQuizInput {
  @Field()
  id: number;
}

@ObjectType()
export class ResultOutputDTO {
  @Field(() => ID)
  questionId: number;

  @Field()
  questionContent: string;

  @Field(() => ID)
  answerId: number;
  
  @Field()
  answerContent: string;
}
