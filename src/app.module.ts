import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { QuestionModule } from './question/question.module';
import { QuizModule } from './quiz/quiz.module';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { AnswerModule } from './answer/answer.module';
import { ResultModule } from './result/result.module';

@Module({
  imports: [
		QuestionModule, QuizModule,
		GraphQLModule.forRoot<ApolloDriverConfig>({
					autoSchemaFile: 'schema.gql',
					driver: ApolloDriver,
		}),
		TypeOrmModule.forRoot({
		type: 'mariadb',
		//host: '127.0.0.1',
		host: '172.17.0.2',
		port: 3306,
		username: 'uzytkownik',
		password: 'pass',
		database: 'test1',
		entities: ['dist/**/*.entity{.ts,.js}'],
		synchronize: true,
    }),
		AnswerModule,
		ResultModule,
   ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
